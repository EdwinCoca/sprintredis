package com.example.demo.controller;

import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.DTO.Student;
import com.example.demo.service.StudentService;

@RestController
public class RestDemoController {

	private final AtomicLong counter = new AtomicLong();

	@Autowired
	private StudentService studentService;

	@GetMapping("/greeting")
	public Student greeting(@RequestParam(value = "name", defaultValue = "World") String name) {

		Student student = new Student(counter.incrementAndGet(), name, "Apellido");
		studentService.createStudent(student);
		return student;
	}

	@GetMapping("/students")
	public Map<Object, Student> searchAll() {
		return studentService.searchAllStudent();
	}

}
