package com.example.demo.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.DTO.Student;
import com.example.demo.repository.IStudentRepository;

@Service
public class StudentService {

	@Autowired
	private IStudentRepository iStudentRepository;

	public void createStudent(Student student) {
		iStudentRepository.add(student);
	}

	public Map<Object, Student> searchAllStudent() {
		return iStudentRepository.findAllStudent();
	}

}
