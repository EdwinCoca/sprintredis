package com.example.demo.cola;

public interface MessagePublisher {

    void publish(final String message);
}
