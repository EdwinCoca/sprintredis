package com.example.demo.repository;

import java.util.Map;

import com.example.demo.DTO.Student;

public interface IStudentRepository {

	public Map<Object, Student> findAllStudent();

	public void add(Student student);

	public void delete(String id);

	public Student findStudent(String id);

}
