package com.example.demo.repository;

import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.example.demo.DTO.Student;

@Repository
@SuppressWarnings({ "rawtypes", "unchecked" })
public class StudentRepository implements IStudentRepository {

	private static final String KEY = "Student";

	private RedisTemplate<String, Object> redisTemplate;

	private HashOperations hashOperations;

	@Autowired
	public StudentRepository(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	@PostConstruct
	private void init() {
		hashOperations = redisTemplate.opsForHash();
	}

	@Override
	public Map<Object, Student> findAllStudent() {
		return hashOperations.entries(KEY);
	}

	@Override
	public void add(Student student) {
		hashOperations.put(KEY, student.getId(), student);
	}

	@Override
	public void delete(String id) {
		hashOperations.delete(KEY, id);
	}

	@Override
	public Student findStudent(String id) {
		return (Student) hashOperations.get(KEY, id);
	}

}
